
/**
 * author: Victor Alejandro Martin Dzib.
 */

let static = 8;
let contador_pizzas = 0;
let tipo_pizza = ['Peperoni', 'Hawaiana', 'Mexicana', 'Champiñones'];
let frase = document.getElementById('piz').textContent;
let vacio = false;

/*funcion que recibe de parametro el color de la pizza y un indice
para determinar la seleccion de pizza*/
function cambiarColorPizza(color, opcion){
    vacio = true;
    document.getElementById("piz").innerHTML = frase + ' ' + tipo_pizza[opcion];
    Array.from(document.querySelectorAll('.reb')).forEach(rebanada => {
        rebanada.style = 'background: ' + color;
    });
    static = 8;
}

/*funcion para comer las rebanadas, recibe de parametro el id
de la rebanada a la que se le dio clic y procede a colorearla */
function comerRebanada(id){
    var rebanada_actual = document.getElementById(id);
    if(!es_gris(rebanada_actual.style.backgroundColor) && vacio){
        static--;
        document.getElementById("reb_com").innerHTML = "Rebanadas restantes: " + static;
    } 
    rebanada_actual.style = 'background: gray';
    static == 0 ? (static = 8, incrementarContadorPizzas()) : (static = static);
}

/*funcion de seleccion, recibe un numero entero del cual 
se basa para mostrar cual fue la seleccion*/
function incrementarContadorPizzas(){
    contador_pizzas++;
    document.getElementById('cantidad').innerHTML = "Pizzas comidas: " + contador_pizzas;
}

/*Valida que la rebanada ya ha sido comida*/
function es_gris(colorR){
    return colorR == 'gray' ? true : false;
}
